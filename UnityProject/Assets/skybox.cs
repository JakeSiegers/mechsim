﻿using UnityEngine;
using System.Collections;

public class skybox : MonoBehaviour {
	
	public GameObject sky;
	private GameObject camera;
	// Use this for initialization
	void Start () {
		camera = GameObject.FindGameObjectWithTag("MainCamera");
		transform.position = sky.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		transform.rotation = camera.transform.rotation;
	}
}

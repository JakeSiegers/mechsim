﻿using UnityEngine;
using System.Collections;

public class MenuBackground : MonoBehaviour {

	private float sinNum = 0f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		sinNum += 0.000001f;
		if (sinNum > Mathf.PI * 2) {
			sinNum = 0f;		
		}

	}
}

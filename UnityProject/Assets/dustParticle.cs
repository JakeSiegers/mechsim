﻿using UnityEngine;
using System.Collections;

public class dustParticle : MonoBehaviour {
	public Animator anim;
	private Transform followPoint;
	// Use this for initialization
	void Start () {
		followPoint = transform.parent;

		transform.SetParent (null);
		transform.rotation = Quaternion.Euler (0, 0, 0);
	}
	
	// Update is called once per frame
	void Update () {
		if (followPoint.Equals (null)) {
						Destroy (gameObject);

				} else if (!followPoint.gameObject.name.Contains ("Leg"))
						Destroy (gameObject);
				else {
						transform.position = followPoint.position;
						//transform.rotation.y = character.rigidbody.velocity
						if (!anim.GetBool ("breaking"))
								particleSystem.enableEmission = false;
						else
								particleSystem.enableEmission = true;
				}
	}
}

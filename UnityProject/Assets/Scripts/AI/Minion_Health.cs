﻿using UnityEngine;
using System.Collections;

public class Minion_Health : MonoBehaviour
{

	public Minion_AI ai;
	public int maxHealth;
	private int health;
	private bool damaged = false;
	private bool isDead = false;
	
	void Start ()
	{
		health = maxHealth;
	}
	
	void Update ()
	{
		
	}
	
	public void TakeDamage (int amount)
	{
		damaged = true;
		
		health -= amount;
		
		if(health <= 0 && !isDead)
		{
			Death ();
		}
	}
	
	
	void Death ()
	{
		isDead = true;
		GameObject.Destroy(gameObject);
	}
}

﻿using UnityEngine;
using System.Collections;

public class Minion_AI : MonoBehaviour
{	
	public enum Minion_State{STATIONARY, PATROLLING, SHOOTING, PURSUING_TARGET, SEARCHING_FOR_TARGET};
	string[] Minion_State_Strings = {"Stationary", "Patrolling", "Shooting", "Pursuing Target", "Searching for Target"};

	public GameObject target = null;
	public GameObject minion = null;
	public GameObject minionMissile = null;
	public Transform shootSource;
	public int checkDeltaFrames = 5;
	public float sightAngle;
	public float sightRange;
	public float nearSightRange;
	public float shootRangeMax;
	public float shootDeltaTime;
	public float shootSpread;
	
	private Minion_State state;
	private Vector3 nextLocation;
	private NavMeshAgent nma;
	private int stateSpecIter = 0;
	private bool newState = true;
	private Vector3 lastTraj;
	private float timer = 0f;
	float distToTarget;
	
	
	// Use this for initialization
	void Start ()
	{
		nma = gameObject.GetComponent<NavMeshAgent>();
		setState (Minion_State.PATROLLING);
		nextLocation = transform.position;
		target = GameObject.Find("Mech Target");
		if(minion == null)
			Destroy(gameObject);
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(minion == null)
		{	
			Destroy(gameObject);
			return;
		}
		switch(state)
		{
		case Minion_State.SHOOTING:
			
			distToTarget = Vector3.Distance(transform.position, target.transform.position);
		    
		    //Check if target still in range
			if(distToTarget >= shootRangeMax)
				setState(Minion_State.PURSUING_TARGET);
			
			timer += Time.deltaTime;
			
			//shootRotations();
			
			if(timer > shootDeltaTime)
			{
				timer = 0;
				shoot ();
			}
			
			if(!targetInSight())
				setState (Minion_State.SEARCHING_FOR_TARGET);
			
			minion.transform.rotation = Quaternion.Slerp (minion.transform.rotation, Quaternion.LookRotation(target.transform.position-minion.transform.position),nma.angularSpeed/25f*Time.deltaTime);
		
			break;
		case Minion_State.PATROLLING:
			if(Vector3.Distance(transform.position, nextLocation) < 10f)
				newPatrolLocation();
			checkForTarget();
			break;
		case Minion_State.PURSUING_TARGET:
			nextLocation = target.transform.position;
			
			distToTarget = Vector3.Distance(transform.position, target.transform.position);
		    //Check for target exiting vision
			
		    if(!(distToTarget <= sightRange/3f) && //Target is close enought to see
		    (Vector3.Angle(transform.forward, (target.transform.position - transform.position)) <= sightAngle || //target is in cone of vision
		 	(distToTarget <= nearSightRange/3f) || !targetInSight())) // or target is close enough to sense
      		{
				setState(Minion_State.SEARCHING_FOR_TARGET);
			}
		
			//Check whether to move or shoot
			if(distToTarget < shootRangeMax)
				setState (Minion_State.SHOOTING);
			
			break;
		case Minion_State.SEARCHING_FOR_TARGET:
			//State Specific Iteration
			// 0 - walk to last known location
			// 1 - look around
			// 2 - walk in last known direction
			if(stateSpecIter == 0 || stateSpecIter == 2)
				nma.SetDestination(nextLocation);
			else
			{
				transform.Rotate(Vector3.up * .2f);
				timer += Time.deltaTime;
				if(timer > 1.5f)
				{
					stateSpecIter++;
					timer = 0f;
					nextLocation = transform.position + (Random.Range(100f,500f) * lastTraj);
				}
			}
			
			if(Vector3.Distance(gameObject.transform.position,nextLocation) < .5f)
			{
				if(stateSpecIter == 1)
				{
					stateSpecIter = 2;
				}
				else
				{
					setState(Minion_State.PATROLLING);
				}
			}
			
			checkForTarget();
			
		break;
		case Minion_State.STATIONARY:
			checkForTarget ();
			break;
		default:
			Debug.Log (gameObject.name + " is in an invalid state");
			break;
		}	
	}
	
	void checkForTarget()
	{
		float distanceToTarget = Vector3.Distance(minion.transform.position, target.transform.position);
		float angleToTarget = Vector3.Angle(transform.forward, (target.transform.position - minion.transform.position));
		if(distanceToTarget <= sightRange && //Current not in sight previously and target is close enought to see
		   (angleToTarget <= sightAngle || //target is in cone of vision
		 distanceToTarget <= nearSightRange))// or target is close enough to sense   
		{
			if(targetInSight())
				setState(Minion_State.PURSUING_TARGET);
		}
		Debug.Log("d: " + distanceToTarget + " a: " + angleToTarget);
	}
	
	void setState(Minion_State s)
	{
		state = s;
		stateSpecIter = 0;
		//What to do at beggining of state
		switch(state)
		{
		case Minion_State.SHOOTING:
			nma.SetDestination(transform.position);
			nma.enabled = false;
			break;
		case Minion_State.PATROLLING:
			nma.enabled = true;
			break;
		case Minion_State.PURSUING_TARGET:
			nma.SetDestination(target.transform.position);
			lastTraj = target.gameObject.GetComponentInParent<Rigidbody>().velocity;
			nma.enabled = true;
			break;
		case Minion_State.SEARCHING_FOR_TARGET:
			nma.enabled = true;
			break;
		case Minion_State.STATIONARY:
			nma.enabled = true;
			break;
		default:
			Debug.Log (gameObject.name + " is in an invalid state");
			break;
		}
	}
	
	void shoot()
	{
		float xSpread, ySpread, zSpread;
		xSpread = Random.Range(-shootSpread, shootSpread);
		ySpread = Random.Range(-shootSpread, shootSpread);
		zSpread = Random.Range(-shootSpread, shootSpread);
		Instantiate(minionMissile, shootSource.position, Quaternion.Euler(shootSource.transform.localEulerAngles.x+90+xSpread, transform.localEulerAngles.y+ySpread, minion.transform.localEulerAngles.z+zSpread));
	}
	
	bool targetInSight()
	{
		RaycastHit hit;
		bool itHit = Physics.Raycast(minion.transform.position, target.transform.position - minion.transform.position, out hit);
		if(itHit)
			Debug.Log (hit.collider.gameObject.name);
		return itHit && hit.collider.gameObject.tag == "Player";
	}
	
	void newPatrolLocation()
	{
		Vector3 randomDirection = Random.insideUnitSphere * 5f;
		NavMeshHit nHit;
		NavMesh.SamplePosition(transform.position+randomDirection, out nHit, 10000f,1);
		nextLocation = nHit.position;
		nma.SetDestination(nextLocation);
	}
	
	//My Struggle
	/*void shootRotations()
	{
		float dX, dY, dZ;
		dX = target.transform.position.x - minion.transform.position.x;
		dY = target.transform.position.y - minion.transform.position.y;
		dZ = target.transform.position.z - minion.transform.position.z;
		
		float desiredYAngle = Mathf.Atan (dX/dZ) * Mathf.Rad2Deg;
		
		if(dZ <= 0)
		{
			desiredYAngle = 180+desiredYAngle;
		}
		
		float desiredXAngle = Mathf.Atan (dY/dZ) * Mathf.Rad2Deg;
		if(dZ <= 0)
		{
			desiredXAngle = 180+desiredYAngle;
		}
		
		Debug.Log("dy: " + dY + " dz: " + dZ);
		Debug.Log("x:" + desiredXAngle + " y: " + desiredYAngle);
		Debug.Log ("Local angles: " + minion.transform.localEulerAngles);
		
		transform.localRotation = Quaternion.Lerp(transform.localRotation, Quaternion.Euler(desiredXAngle ,desiredYAngle,transform.localEulerAngles.z), nma.angularSpeed/25 * Time.deltaTime);
		//minion.transform.localRotation = Quaternion.Lerp(minion.transform.localRotation, Quaternion.Euler(desiredXAngle,minion.transform.localEulerAngles.y,minion.transform.localEulerAngles.z), nma.angularSpeed/10 * Time.deltaTime);
		
		
	}*/
}
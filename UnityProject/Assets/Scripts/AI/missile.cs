﻿using UnityEngine;
using System.Collections;

public class missile : MonoBehaviour
{

	public float speed, explosionPower, explosionRadius;
	public float damage;
	public GameObject explosion;
	
	// Update is called once per frame
	void Update ()
	{
		rigidbody.velocity = (transform.up * speed);
	}
	
	void OnCollisionEnter(Collision col)
	{
		if(col.gameObject.tag != "Enemy")
		{
			Instantiate(explosion, transform.position, Quaternion.identity);
			Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius);
			col.collider.gameObject.SendMessage("TakeDamage", damage );
			foreach(Collider c in colliders)
			{
				if(c.rigidbody != null)
				{
					c.rigidbody.AddExplosionForce(explosionPower, transform.position, explosionRadius, 1f, ForceMode.Impulse);
					if(c.gameObject.tag == "Player")
					{
						c.gameObject.SendMessage("TakeDamage", damage );
					}
				}
			}
			
			Destroy(gameObject);
		}
	}
}

﻿using UnityEngine;
using System.Collections;

public class DestoryAfterTIme : MonoBehaviour
{
	public float lifeTime;
	
	void Start ()
	{
		Destroy(gameObject, lifeTime);
	}
}

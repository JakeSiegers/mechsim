﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class turret : MonoBehaviour
{
	enum TurretState{targetAcquired, looking};

	public GameObject turretChasis;
	public GameObject turretGun;
	public string playerTag;
	public float turnSpeed;
	public float lookingRotateSpeed;
	public GameObject ammo;
	public GameObject[] ammoSpawnPoints;
	public float roundPerMinute;
	
	private float deltaShootTime, shootTime;
	private TurretState state;
	private List<GameObject> players;
	private short spawnPointIndex;

	// Use this for initialization
	void Start ()
	{
		deltaShootTime = 60.0f/roundPerMinute;
		state = TurretState.looking;
		players = new List<GameObject>();
		spawnPointIndex = 0;
		shootTime = 0;
	}
	
	// Update is called once per frame
	void Update ()
	{
		switch(state)
		{
			case TurretState.looking: //Rotate
				turretChasis.transform.RotateAround(transform.position, Vector3.up, lookingRotateSpeed * Time.deltaTime);
				break;
			case TurretState.targetAcquired:
				//----Rotate chasis (xz-plane)----
				float dxChasis, dzChasis, desiredChasisAngle, angleMoveThisFrame;
				
				dxChasis = turretChasis.transform.position.x - players[0].transform.position.x;
				dzChasis = turretChasis.transform.position.z - players[0].transform.position.z;
				
				desiredChasisAngle = Mathf.Atan(dxChasis/dzChasis) * Mathf.Rad2Deg;
				//Account for Range of arctangent function
				if(dxChasis >= 0 && dzChasis < 0)
				{
					desiredChasisAngle = 180 + desiredChasisAngle;
				}
				else if(dxChasis < 0 && dzChasis < 0)
				{
					desiredChasisAngle = 180 + desiredChasisAngle;
				}
				else if(dxChasis < 0 && dzChasis >= 0)
				{
					desiredChasisAngle = 360 + desiredChasisAngle;
				}
				
				angleMoveThisFrame = turnSpeed * Time.deltaTime;
	
				turretChasis.transform.localRotation = Quaternion.Lerp(turretChasis.transform.localRotation, Quaternion.Euler(0,0,desiredChasisAngle), angleMoveThisFrame);
					
				//----Rotate gun (xy-plane)----
				float dyGun, dxzGun, desiredGunAngle;
				
				dyGun = players[0].transform.position.y - turretGun.transform.position.y;
				dxzGun = Mathf.Abs (Vector2.Distance(new Vector2(players[0].transform.position.x, players[0].transform.position.z), new Vector2(turretGun.transform.position.x, turretGun.transform.position.z)));
				
				desiredGunAngle = Mathf.Atan(dyGun/dxzGun) * Mathf.Rad2Deg;
				
				desiredGunAngle = Mathf.Clamp(desiredGunAngle,-30f,40f);
				turretGun.transform.localRotation = Quaternion.Lerp(turretGun.transform.localRotation, Quaternion.Euler(desiredGunAngle,0,0), angleMoveThisFrame);
				
				//----SHOOT----
				RaycastHit rHit;
				shootTime += Time.deltaTime;
				if(shootTime >= deltaShootTime && 
					Physics.Raycast(turretGun.transform.position, turretGun.transform.up, out rHit, gameObject.GetComponent<SphereCollider>().radius+5, 9)
					&& rHit.rigidbody != null && rHit.rigidbody.tag.Equals("Player"))
				{
					Instantiate(ammo, ammoSpawnPoints[spawnPointIndex++].transform.position, turretGun.transform.rotation);
					if(spawnPointIndex >= ammoSpawnPoints.Length)
						spawnPointIndex = 0;
					shootTime = deltaShootTime/2;
				}
				
				break;//End target acquired state
		}
	}
	
	void OnTriggerEnter(Collider col)
	{
		if(col.gameObject.tag.Equals(playerTag))
		{
			players.Add(col.gameObject);
			if(players.Count == 1) //Player is only target, previously there was no target
				state = TurretState.targetAcquired;
		}
	}
	
	void OnTriggerExit(Collider col)
	{
		if(col.gameObject.tag.Equals(playerTag))
		{
			players.Remove(col.gameObject);
			if(players.Count == 0) //No Targets, return to rotation
			{
				state = TurretState.looking;
				shootTime = 0f;
			}
		}
	}
}

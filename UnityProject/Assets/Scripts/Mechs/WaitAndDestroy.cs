﻿using UnityEngine;
using System.Collections;

public class WaitAndDestroy : MonoBehaviour {

	public float lifetime = 10f;

	void Start () 
	{
		Destroy (gameObject, lifetime);
	}
	

}

﻿using UnityEngine;
using System.Collections;

public class MechShooting1 : MonoBehaviour 
{
	public float dmgPerShot = 2;
	public float lifetime = .15f;
	public float range = 100f;
	public float cooltime = 2.5f;
	public Object Bullet = new Object();
	public Object bullethole = new Object();
	public float startingHeat = 0f;
	public float currentHeat;
	public bool leftGun = false;
	public float strayFactor = .25f;
	
	float timer;
	Ray shootRay;
	RaycastHit shootHit;
	int shootableMask;
	float lastUpdate;
	MechHUD HUD;
	ParticleSystem gunParts;
	AudioSource gunAudio;
	Light gunLight;
	float FXDisplayTime = 0.2f;
	Object cloneBullet = new Object();
	Object cloneHole = new Object();
	private string button;

	
	void Awake () 
	{
		gunParts = GetComponent<ParticleSystem> ();
		gunAudio = GetComponent<AudioSource> ();
		gunLight = GetComponent<Light> ();

		Instantiate (Bullet);
		Instantiate (bullethole);
		if (leftGun)
			button = "Fire1";
		else
			button = "Fire2";

	}
	
	void Update () 
	{
		timer += Time.deltaTime;


		if (Input.GetButton (button) && timer >= lifetime && currentHeat < 50) 
		{
			Shoot ();
		}
		else if (!Input.GetButton (button) && timer >= cooltime)
		{
			Cooldown();		
		}
		
		if (timer >= lifetime * FXDisplayTime) 
		{
			DisableFX ();	
		}

		//Destroy (cloneHole,holeLife);
	}

	public void DisableFX()
	{
		gunLight.enabled = false;
	}
	
	public void Shoot ()
	{
		timer = 0f;
		
		gunAudio.Play ();
		
		gunLight.enabled = true;
		
		gunParts.Stop ();
		gunParts.Play ();

		

		shootRay.origin = transform.position;
		Transform shootTransform = transform;
		shootTransform.Rotate(0, Random.Range (-strayFactor, strayFactor), 0);
		shootRay.direction = shootTransform.forward;
		cloneBullet = Instantiate (Bullet, transform.position, shootTransform.rotation);
		
		if(Physics.Raycast (shootRay, out shootHit, range))
		{
			MechHealth enemyHealth = shootHit.collider.GetComponent <MechHealth> ();
			if(enemyHealth != null)
			{
				enemyHealth.TakeDamage (dmgPerShot);
			}

			shootRay = Camera.main.ScreenPointToRay (Input.mousePosition);
			var hitRotation = Quaternion.FromToRotation(Vector3.up, shootHit.normal);
			if(shootHit.transform.tag == "Environment")
			{
				
				cloneHole = Instantiate (bullethole, shootHit.point, hitRotation);
			}
		}

		if (currentHeat >= 48f)
		{
			currentHeat = 50f;
		}
		else if (currentHeat >= 0f && currentHeat < 48f)
		{
			currentHeat += 2f;
		}
	}
	
	public void setDmg(float dmg)
	{
		dmgPerShot = dmg;
	}

	public void setFireRate(float rate)
	{
		lifetime = rate;
	}

	public void Cooldown()
	{
		if (currentHeat >= 0.5f)
		{
			currentHeat -= 0.5f;
		}
		else
		{
			currentHeat = startingHeat;
		}
	}
}


using UnityEngine.UI;
using UnityEngine;
using System.Collections;

public class MechHUD : MonoBehaviour
{
	public int startingHealth = 100;
	public int currentHealth;
	public int startingHeat = 0;
	public int currentHeat;
	public AudioClip deathClip;
	
	public GameObject leftArm;
	public GameObject rightArm;
	public GameObject heatLeft;
	public GameObject heatRight;
	public GameObject Health;
	
	float timer = 0f;
	Animator anim;
	AudioSource mechAudio;
	MechMove mechMove;
	bool isDead;
	bool damaged;
	bool shooting;
	MechShooting1 mechShoot1;
	MechShooting1 mechShoot2;
	Slider HealthSlider;
	Slider heatSliderLeft;
	Slider heatSliderRight;
	MechHealth healthScript;
	GameObject mechHud;
	
	
	void Start ()
	{
		
		heatRight = GameObject.Find("HeatBarRight");
		heatLeft = GameObject.Find ("HeatBarLeft");
		Health = GameObject.Find ("healthy");
		heatSliderLeft = heatLeft.GetComponent<Slider>();
		heatSliderRight = heatRight.GetComponent<Slider>();
		healthScript = gameObject.GetComponent<MechHealth>();
		HealthSlider = Health.GetComponent<Slider>();
		
		
		anim = GetComponent <Animator> ();
		mechAudio = GetComponent <AudioSource> ();
		mechMove = GetComponent <MechMove> ();
		currentHealth = startingHealth;
		mechShoot1 = leftArm.GetComponent<MechShooting1>();
		mechShoot2 = rightArm.GetComponent<MechShooting1>();
	}
	
	
	void Update ()
	{
		damaged = false;
		
		heatSliderLeft.value = mechShoot1.currentHeat;
		heatSliderRight.value = mechShoot2.currentHeat;
		HealthSlider.value = healthScript.getCurrHealth();
		HealthSlider.maxValue = healthScript.getMaxHealth();
	}
	
	
}

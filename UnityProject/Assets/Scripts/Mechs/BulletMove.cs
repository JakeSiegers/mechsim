﻿using UnityEngine;
using System.Collections;

public class BulletMove : MonoBehaviour 
{
	public float speed = 10;
	public float lifetime = 10;
	public float strayFactor = 1f;
	private float startTime;
	public Object bullethole = new Object();
	void Start () 
	{
		startTime = Time.time;
		//transform.Rotate (0, Random.Range (-strayFactor, strayFactor), 0);
	}

	void FixedUpdate()
	{
		//this.gameObject.transform.position += speed * this.gameObject.transform.forward;

		rigidbody.AddForce (transform.forward * speed, ForceMode.Impulse);
		if (Time.time - startTime >= lifetime) 
		{
			Destroy (this.gameObject);
		}
	}

	void OnCollisionEnter (Collision shootHit)
	{
		Destroy (this.gameObject);
	}
		
}

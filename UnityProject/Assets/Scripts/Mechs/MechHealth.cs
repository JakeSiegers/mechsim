﻿using UnityEngine;
using System.Collections;

public class MechHealth : MonoBehaviour {

	public GameObject rm;
	private RoundManager manager;
	public float defaultHealth = 100f;
	private float maxHealth = 100f;
	private float currentHealth = 100f;
	
	// Use this for initialization
	void Start () {
		rm = GameObject.FindGameObjectWithTag("MainCamera");
		manager = rm.GetComponent<RoundManager>();
		maxHealth = defaultHealth;
		currentHealth = defaultHealth;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (Input.GetKey(KeyCode.Escape))
			TakeDamage(5);
	}

	public void TakeDamage (float amount)
	{
		
		currentHealth -= amount;
		
		//HealthSlider.value = currentHealth;
		
		//mechAudio.Play ();
		
		if(currentHealth <= 0)
		{
			die ();
		}
	}
	
	public float getMaxHealth()
	{
		return maxHealth;
	}

	public float getCurrHealth()
	{
		return currentHealth;
	}
	
	public void heal(float heal)
	{
		currentHealth+=heal;
		if(currentHealth>maxHealth)
			currentHealth = maxHealth;
	}

	public void setHealth(float newMaxHealth)
	{
		maxHealth = newMaxHealth;
	}

	void die ()
	{
		manager.die(this.gameObject);
	}
}

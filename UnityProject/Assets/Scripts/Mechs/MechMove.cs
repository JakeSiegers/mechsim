using UnityEngine;
using System.Collections;

public class MechMove : MonoBehaviour {

	//------Character Movement---------
	private bool isControllable;
	public float horzSpeed;
	public float vertSpeed;
	public float decelSpeed;
	public float speedMod = 1;

	public float jumphorz = 30;
	public float jumpvert = 30;

	public float wheelhorz = 10;
	public float wheelvert = 60;

	public float jumpHeight = 10;
	private bool jumpBtn = false;

	public float jumpMinStr = 1;
	private float jumpHoldStr = 1;
	public float jumpMaxStr = 3;

	public float maxVelocity = 20.0f;
	private bool grounded = false;

	public float turnAngle = 30f;
	

	//-----Camera--------

	private GameObject cam;
	private float cursorX;
	private float cursorY;
	public float camDistX = 12;
	public float camDistY = 12;
	public float xSpeed = 20;
	public float ySpeed = 20;
	public float camHeightDrag = 5.0f;
	public float camSwing = 0.0f;

	//-----Parts------

	public string torso;
	public string head;
	public string legs;
	public Color primary;
	public Color secondary;
	

	//-----Animation------

	public Animator anim;
	public GameObject leftArm;
	public GameObject rightArm;
	public float minAngle = -30f;
	public float maxAngle = 30f;

	// Use this for initialization



	void Start () 
	{

		

		if (legs.Contains ("Wheel")) 
		{
			horzSpeed = wheelhorz;
			vertSpeed = wheelvert;
			maxVelocity = 10;
			jumpHeight = jumpHeight/3;

		}
		else if (legs.Contains ("Jump")) 
		{
			horzSpeed = jumphorz;
			vertSpeed = jumpvert;
			maxVelocity = 6;
		}




		//statsObj.resetSpeed(vertSpeed);

		isControllable = false; //networkView.isMine
		Vector3 angles = transform.eulerAngles;
		cursorX = angles.x;
		cursorY = angles.y;
		if (gameObject.transform.rotation != Quaternion.identity) {
						leftArm.transform.rotation = Quaternion.Euler (angles.x, angles.y, angles.z - 90);
						rightArm.transform.rotation = Quaternion.Euler (angles.x, angles.y, angles.z - 90);
				}

		if (isControllable)
			cam = GameObject.FindWithTag ("MainCamera");

		
		foreach (Transform child in gameObject.GetComponentsInChildren<Transform>()) 
		{
				if(child.gameObject.name.Contains("Torso") || child.gameObject.name.Contains("Legs") || child.gameObject.name.Contains("Head"))
					if(!(child.gameObject.name == torso || child.gameObject.name == legs || child.gameObject.name == head))
					{
						GameObject.Destroy (child.gameObject);
					}
				if(child.GetComponent<SkinnedMeshRenderer>()!= null)
				{

					Mesh oldMesh = child.GetComponent<SkinnedMeshRenderer>().sharedMesh;
					Mesh newMesh = (Mesh)Object.Instantiate(oldMesh);
					
					Color[] colors = new Color[newMesh.colors.Length];
					for (var i = 0; i < newMesh.colors.Length; i++){
						if(newMesh.colors[i].r <.5)
							colors[i] = primary;
						else
							colors[i] = secondary;  
					}
					newMesh.colors = colors;
					child.GetComponent<SkinnedMeshRenderer>().sharedMesh = newMesh;
			}
		}


	}
	
	// Update is called once per frame
	void Update () 
	{

		if (Input.GetKey(KeyCode.Escape))
			Screen.lockCursor = false;
		else
			Screen.lockCursor = true;
	
		if (isControllable) {
						if (Input.GetButton ("Jump")) {
								if (jumpHoldStr < jumpMaxStr) {
										jumpHoldStr += Time.deltaTime;
								}
						}
						if (Input.GetButtonUp ("Jump")) {
								jumpBtn = true;
						}
						cursorX += (float)(Input.GetAxis ("Mouse X") * xSpeed * 0.02f);
						transform.rotation = Quaternion.Euler (0, cursorX, 0);
				} 
				else
						Debug.Log (isControllable);
	}
	
	void FixedUpdate ()
	{
	
		float z = transform.InverseTransformDirection (rigidbody.velocity).z;
		float x = transform.InverseTransformDirection (rigidbody.velocity).x;
		anim.SetFloat("horizontalVelocity", x);
		anim.SetFloat("forwardVelocity", z);
		anim.SetFloat ("forwardOverHorz", Mathf.Abs(z/x));
		anim.SetBool ("jumping", !grounded);
		

		Vector3 forward = transform.forward;
		Vector3 right = new Vector3 (forward.z, 0, -forward.x);

		float vertical = 0;
		float horizontal = 0;
		if(isControllable)
		{
			vertical = Input.GetAxis ("Vertical") * vertSpeed;
			horizontal = Input.GetAxis ("Horizontal") * horzSpeed;
		}

		Vector3 targDirection = (forward * vertical + right * horizontal) * Time.deltaTime;
		if (grounded && ((Mathf.Abs(vertical)+Mathf.Abs(horizontal))<.01 || Vector3.Angle(rigidbody.velocity,targDirection)>turnAngle  || jumpHoldStr > jumpMinStr)&&rigidbody.velocity!= Vector3.zero)
		{
			//Vector3 brakeDirection = new Vector3 (rigidbody.velocity.x, 0, rigidbody.velocity.z);
			//rigidbody.AddForce (-brakeDirection / decelSpeed, ForceMode.VelocityChange);
			rigidbody.velocity = rigidbody.velocity * (1- 1/decelSpeed);
			anim.SetBool ("breaking", true);
			if(rigidbody.velocity.magnitude<.05)
			{
				anim.SetBool("breaking",false);
				rigidbody.velocity = Vector3.zero;
			}

		
		} 
		else 
		{
			anim.SetBool ("breaking", false);
		}



		targDirection.y = 0;
		if (!grounded) {
						targDirection = targDirection / 1f;
				} else if (jumpHoldStr > jumpMinStr)
						targDirection = targDirection / 2;

		rigidbody.AddForce(targDirection, ForceMode.VelocityChange);


		Vector3 yCheck = new Vector3 (0, rigidbody.velocity.y, 0);
		Vector3 velCheck = new Vector3 (rigidbody.velocity.x, 0, rigidbody.velocity.z);
		if (velCheck.magnitude > maxVelocity *speedMod) 
		{
			rigidbody.velocity = yCheck + velCheck.normalized * maxVelocity*speedMod;
		}



		if (jumpBtn && grounded) 
		{
			rigidbody.velocity = new Vector3(rigidbody.velocity.x, jumpHeight * jumpHoldStr, rigidbody.velocity.z);
			jumpHoldStr = jumpMinStr;

		}



		jumpBtn = false;
	}
	
	void LateUpdate ()
	{

		if (isControllable) 
		{
			leftArm.transform.rotation = Quaternion.Euler (-cursorY, leftArm.transform.eulerAngles.y, leftArm.transform.eulerAngles.z );
			rightArm.transform.rotation = Quaternion.Euler (-cursorY, rightArm.transform.eulerAngles.y, rightArm.transform.eulerAngles.z );

			cursorY -= (float)(Input.GetAxis("Mouse Y") * ySpeed * 0.02f);
			cursorY = ClampAngle (cursorY, minAngle, maxAngle );

			float targRotationAngleY = transform.eulerAngles.y;
			float currentRotationAngleY = cam.transform.eulerAngles.y;

			float targRotationAngleX = transform.eulerAngles.x;
			float currentRotationAngleX = cam.transform.eulerAngles.x;

			float targHeight = transform.position.y;
			float currentHeight = cam.transform.position.y;
			//currentHeight = Mathf.SmoothDampAngle (currentHeight, targHeight, ref camHeightDrag, .5f);


			currentRotationAngleY = Mathf.SmoothDampAngle (currentRotationAngleY, targRotationAngleY, ref camSwing, .1f);
			Vector3 position = transform.position+ transform.up * camDistY;
			position-= Quaternion.Euler (cursorY+15,currentRotationAngleY , 0) * Vector3.forward * camDistX;
			//position.y = targHeight;
			cam.transform.position = position;
			cam.transform.LookAt (transform.position + transform.up * camDistY);

		}
	}
	private static float ClampAngle(float angle, float min, float max) {
		if (angle < -360)
			angle += 360;
		if (angle > 360)
			angle -= 360;
		return Mathf.Clamp (angle, min, max);
	}


	//Collisions

	void OnCollisionEnter(Collision collision)
	{
		grounded = false;
		foreach (ContactPoint contact in collision.contacts) 
		{
			Vector3 normal = contact.normal;
			if(Vector3.Angle(normal,transform.up)<40)
				grounded = true;
		}

	}
	void OnCollisionStay(Collision collision)
	{
		grounded = false;
		foreach (ContactPoint contact in collision.contacts) 
		{
			Vector3 normal = contact.normal;
			if(Vector3.Angle(normal,transform.up)<40)
				grounded = true;
		}
		
	}
	public void getControl()
	{
		cam = GameObject.FindWithTag ("MainCamera");
		isControllable = true;
		Screen.lockCursor = false;
	}
	public void setSpeed(float newMaxVelocity)
	{
		speedMod = newMaxVelocity/maxVelocity;
	}

	public void loseControl()
	{
		isControllable = false;
		Screen.lockCursor = true;
	}

	void OnCollisionExit()
	{
		grounded = false;
	}
}

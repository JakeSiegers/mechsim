﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class characterStats
{
	//private 
	private static characterStats _instance;
	//The following numbers may need a balance.

	private List<string> upgradeTypes = new List<string> ();

	private float roundTo = 100; //round to 100's place


	private float startingHealth = 100;
	private float health;
	private float minHealth = 10;
	// EXAMPLE: a 1-10% upgrade with a downgrade for another category if the percent is over 6%
	private float minHealthUpgrade = 0.01f;
	private float maxHealthUpgrade = 0.10f;
	private float balanceHealthUpgrade = 0.06f;
	
	private float startingAttackDamage = 10;
	private float attackDamage;
	private float minAttackDamage = 1;
	private float minAttackDamageUpgrade = 0.01f;
	private float maxAttackDamageUpgrade = 0.10f;
	private float balanceAttackDamageUpgrade = 0.06f;

	private float startingSpeed = 8;
	private float speed;
	private float minSpeed=1;
	private float minSpeedUpgrade = 0.01f;
	private float maxSpeedUpgrade = 0.10f;
	private float balanceSpeedUpgrade = 0.06f;

	private float startingFireRate = 1;
	private float fireRate;
	private float minFireRate = 0.1f;
	private float minFireRateUpgrade = 0.01f;
	private float maxFireRateUpgrade = 0.10f;
	private float balanceFireRateUpgrade = 0.06f;

	public characterStats(){
		this.upgradeTypes.Add ("Health");
		this.upgradeTypes.Add ("Speed");
		this.upgradeTypes.Add ("Attack");
		this.upgradeTypes.Add ("Fire Rate");
	}
	
	// Use this for initialization
	void Start ()
	{
		this.resetSpeed ();
		this.resetAttack ();
		this.resetHealth ();
		this.resetFireRate ();
		//
		Debug.Log ("Welcome to upgrades!");
		//Debug.Log (generateUpgrade().ToString());

	}

	
	// Update is called once per frame
	void Update ()
	{
		
	}

	public static characterStats Instance
	{
		get
		{
			if (_instance == null)
			{
				_instance = new characterStats();
			}
			
			return _instance;
		}
	}
	
	//returns a 2d array of random upgrade options.
	public List<List<float>> generateUpgrade ()
	{
		List<List<float>> upgrades = new List<List<float>>();
		upgrades.Add (generateUpgradePercents (minHealthUpgrade, maxHealthUpgrade, balanceHealthUpgrade)); 
		upgrades.Add (generateUpgradePercents (minSpeedUpgrade, maxSpeedUpgrade, balanceSpeedUpgrade)); //speed: a 5-40% upgrade with a downgrade for another category if the percent is over 30%
		upgrades.Add (generateUpgradePercents (minAttackDamageUpgrade, maxAttackDamageUpgrade, balanceAttackDamageUpgrade)); //damage: a 5-40% upgrade with a downgrade for another category if the percent is over 30%
		upgrades.Add (generateUpgradePercents (minFireRateUpgrade, maxFireRateUpgrade, balanceFireRateUpgrade)); //fireRate: a 5-40% upgrade with a downgrade for another category if the percent is over 30%
		return upgrades;
	}

	//returns an array, index 1 = upgrade number, index 2, downgrade of another field number.
	public List<float> generateUpgradePercents(float low,float high, float balance)
	{
		List<float> upgrade = new List<float>();
		float percentUpgrade = Mathf.Round(Random.Range(low,high)*this.roundTo)/this.roundTo; //upgrade possible.
		float percentDowngradeBalance = 0;
		if (percentUpgrade > balance) 
		{
			percentDowngradeBalance = Mathf.Round((percentUpgrade-balance)*this.roundTo)/this.roundTo; //a bit of balance if we generate them a nice upgrade.			
		}
		upgrade.Add (percentUpgrade);
		upgrade.Add (percentDowngradeBalance);
		return upgrade;
	}

	//applies the upgrade or downgrade. Could use multiplier for a "double upgrade/downgrade" event or level.
	public void applyUpgradeOrDowngrade(string type,float percent,int multiplier) //multiplier is 1/-1 for upgrade or downgrade
	{
		switch (type) {
			case "health":
				this.health += (this.health*percent*multiplier);
				if(this.health < this.minHealth){
					this.health = this.minHealth;
				}
				break;
			case "speed":
				this.speed += (this.speed*percent*multiplier);
				if(this.speed < this.minSpeed){
					this.speed = this.minSpeed;
				}
				break;
			case "attackDamage":
				this.attackDamage += (this.attackDamage*percent*multiplier);
				if(this.attackDamage < this.minAttackDamage){
					this.attackDamage = this.minAttackDamage;
				}
				break;
			case "fireRate":
				this.fireRate += (this.fireRate*percent*multiplier);
				if(this.fireRate < this.minFireRate){
					this.fireRate = this.minFireRate;
				}
				break;
		}
	}

	//various getters and reseters.

	public void resetSpeed (float resetTo = 0f)
	{
		if (resetTo != 0f) {
			this.startingSpeed = resetTo;
		}
		this.speed = this.startingSpeed;

	}
	
	public void resetAttack (float resetTo = 0f)
	{
		if (resetTo != 0f) {
			this.startingAttackDamage = resetTo;
		}
		this.attackDamage = this.startingAttackDamage;
	}
	
	public void resetHealth (float resetTo = 0f)
	{
		if (resetTo != 0f) {
			this.startingHealth = resetTo;
		}
		this.health = this.startingHealth;
	}

	public void resetFireRate(float resetTo = 0f)
	{
		if (resetTo != 0f) {
			this.startingFireRate = resetTo;
		}
		this.fireRate = this.startingFireRate;
	}

	public float getSpeed ()
	{
		return this.speed;
	}
	
	public float getAttack ()
	{
		return this.attackDamage;
	}
	
	public float getHealth ()
	{
		return this.health;
	}

	public float getFireRate()
	{
		return this.fireRate;
	}
	
	public void resetAll(float speed, float attack,float health, float fireRate)
	{
		startingHealth = health;
		health = startingHealth;
		minHealth = startingHealth/10;

		
		startingAttackDamage = attack;
		attackDamage = startingAttackDamage;
		minAttackDamage = startingAttackDamage/10;
	
		
		startingSpeed = speed;
		speed = startingSpeed;
		minSpeed = startingSpeed/1.5f;
	
		
		startingFireRate = fireRate;
		fireRate = startingFireRate;
		minFireRate = startingFireRate/10;

	}


	public List<string> getUpgradeTypes()
	{
		return this.upgradeTypes;
	}
	
	
	
	
	
}
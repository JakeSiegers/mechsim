using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class characterStatTester : MonoBehaviour {


	private characterStats statsObj;
	public MechMove move;
	private MechShooting1 shootLeft;
	private MechShooting1 shootRight;
	public MechHealth health;
	private List<List<float>> upgrades;
	private List<string> downgrades;
	public GameObject leftArm;
	public GameObject rightArm;
	private bool hasUpgrade;

	public Text upgradeText;

	// Use this for initialization
	void Start () {
		hasUpgrade = false;
		shootLeft = leftArm.GetComponent<MechShooting1>();
		shootRight = rightArm.GetComponent<MechShooting1>();
		statsObj = characterStats.Instance;
		//statsObj.resetAll (move.maxVelocity, shootLeft.dmgPerShot, health.getMaxHealth (), shootRight.lifetime);


	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Alpha1))
			selectUpgradeChoice(1);
		else if(Input.GetKeyDown(KeyCode.Alpha2))
			selectUpgradeChoice(2);
		else if(Input.GetKeyDown(KeyCode.Alpha2))
			selectUpgradeChoice(3);

	}

	public void pickedUpUpgrade(){
	//d ("============================================");
		hasUpgrade = true;
		upgrades= statsObj.generateUpgrade();
		downgrades = new List<string>();
		downgrades.Add ("");
		downgrades.Add ("");
		downgrades.Add ("");
		downgrades.Add ("");
		string textlol = "";
		for(int i=0;i<upgrades.Count;i++){
			string stat = statsObj.getUpgradeTypes()[i];
			string down = "";
			if(upgrades[i][1]>0f){
				List<string> upgradesStats = new List<string>();
				for(int x=0;x<statsObj.getUpgradeTypes().Count;x++){
					upgradesStats.Add (statsObj.getUpgradeTypes()[x]);
				}
				upgradesStats.RemoveAt(i);
				float reduceIndex = Mathf.Floor(Random.Range(0f,upgradesStats.Count));
				string reduction = upgradesStats[(int)reduceIndex];
				down = "(With downgrade of: "+upgrades[i][1]+" for "+reduction+")";
				downgrades[i] = reduction;
			}else{
				down = "";
				//d (i);
				downgrades[i] = "";
			}
			string textLine = "Upgrade "+stat+": "+upgrades[i][0]+" "+down;
			//d (textLine);
			textlol += textLine+"\n";
		}
		upgradeText.text = textlol;

		//upgradeText.text = "HELLO";
		
	}

	public void selectUpgradeChoice(int num){
		if (!hasUpgrade) {
			return;
		}
		upgradeText.text = "Selected to Upgrade: " + statsObj.getUpgradeTypes () [num] 
			+ "\n Percent UP" + upgrades [num] [0]
			+ "\n Percent DOWN" + upgrades [num] [1]
			+"\nCurrent Health = " + statsObj.getHealth ()
			+"\nCurrent Attack = " + statsObj.getAttack ()
			+"\nCurrent Firerate = " + statsObj.getFireRate ()
			+"\nCurrent Speed = "+statsObj.getSpeed();
		hasUpgrade = false;
		//upgrade
		statsObj.applyUpgradeOrDowngrade(statsObj.getUpgradeTypes()[num],upgrades[num][0],1);
		//downgrade
		if (!downgrades [num].Equals ("")) {
			statsObj.applyUpgradeOrDowngrade (downgrades [num], upgrades [num] [1], -1);
		}
		applyUpgrade();

	}

	void OnGUI()
	{
		
		/*if (GUI.Button (new Rect (10, 10, 100, 50), "Switch Scene")) 
		{
			switch (Application.loadedLevelName)
			{
			case "scene1":
				Application.LoadLevel("scene2");
				Debug.Log ("switch to scene2");
				break;
			case "scene2":
				Application.LoadLevel("scene1");
				Debug.Log ("switch to scene1");
				break;
			}
			
		}
		*/
		/*
		if (GUI.Button (new Rect (10, 80, 100, 50), "Get Upgrade")) 
		{


			
		}	
		*/
		/*
		if (GUI.Button (new Rect (10, 160, 100, 50), "Read")) 
		{
			
			Debug.Log ("read: " + characterStats.Instance.getName());
			
		}
		*/
		
	}
	private void applyUpgrade()
	{
		health.setHealth(statsObj.getHealth ());
		move.setSpeed(statsObj.getSpeed ());

		shootLeft.setDmg(statsObj.getAttack());
		shootLeft.setFireRate(statsObj.getFireRate());
		shootRight.setDmg(statsObj.getAttack());
		shootRight.setFireRate(statsObj.getFireRate());
		
	}

	void d(object msg){
		Debug.Log (msg);
	}
}

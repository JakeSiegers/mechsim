﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SpriteAnimator : MonoBehaviour
{

	public Sprite[] sprites;
	public float interframeTime;
	
	private float timer = 0f;
	private int index = 0;
	private Image image;
	
	void Start()
	{
		image = gameObject.GetComponent<Image>();
	}
	
	void Update ()
	{
		timer+= Time.deltaTime;
		if(timer > interframeTime)
		{
			timer -= interframeTime;
			if(++index >= sprites.Length)
				index = 0;
			image.sprite = sprites[index];
		}
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MechSelectButtonListener : MonoBehaviour {

	public RectTransform baseArrow;
	public RectTransform bodyArrow;
	public RectTransform pArrow;
	public RectTransform sArrow;

	float count = 0f;
	//int baseArrowPos = 0;
	//int bodyArrowPos = 0;
	//int pArrowPos = 0;
	//int sArrowPos = 0;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		count += 0.05f;
		if (count > 2*Mathf.PI) {

			count = 0;
		}
		float waveAdjust = -5 + 10 * Mathf.Sin (count);
		//baseArrow.transform.position = pos;
		List<int> positions = new List<int>();
		positions.Add (-44);
		positions.Add (-111);
		positions.Add (-179);
		positions.Add (-248);


		baseArrow.anchoredPosition = new Vector2(-440+waveAdjust,positions[MechSettings.mechBase]);
		baseArrow.rotation = Quaternion.Euler(new Vector3(0,-10+Mathf.Sin(count)*20,0));

		bodyArrow.anchoredPosition = new Vector2(-225+waveAdjust,positions[MechSettings.mechBody]);
		bodyArrow.rotation = Quaternion.Euler(new Vector3(0,-10+Mathf.Sin(count)*20,0));

		pArrow.anchoredPosition = new Vector2(-10+waveAdjust,positions[MechSettings.primaryColor]);
		pArrow.rotation = Quaternion.Euler(new Vector3(0,-10+Mathf.Sin(count)*20,0));

		sArrow.anchoredPosition = new Vector2(221+waveAdjust,positions[MechSettings.secondaryColor]);
		sArrow.rotation = Quaternion.Euler(new Vector3(0,-10+Mathf.Sin(count)*20,0));
		//Debug.Log (pos);
	}

	public void ClickBTN1(){
		Debug.Log("CLICKED BTN 1");
	}

	public void clickBtn(string btnName){

		switch (btnName) {
			case "baseCrab":
				//baseArrowPos = 0;
				MechSettings.mechBase = 0;
				break;
			case "baseWheel":
				//baseArrowPos = 1;
				MechSettings.mechBase = 1;
				break;
			case "bodyLight":
				//bodyArrowPos = 0;
				MechSettings.mechBody = 0;
				break;
			case "bodyMedium":
				//bodyArrowPos = 1;
				MechSettings.mechBody = 1;
				break;
			case "bodyHeavy":
				//bodyArrowPos = 2;
				MechSettings.mechBody = 2;
				break;
			case "pRed":
				//pArrowPos = 0;
				MechSettings.primaryColor = 0;
				break;
			case "pGreen":
				//pArrowPos = 1;
				MechSettings.primaryColor = 1;
				break;
			case "pYellow":
				//pArrowPos = 2;
				MechSettings.primaryColor = 2;
				break;
			case "pBlue":
				//pArrowPos = 3;
				MechSettings.primaryColor = 3;
				break;
			case "sRed":
				//sArrowPos  = 0;
				MechSettings.secondaryColor = 0;
				break;
			case "sGreen":
				//sArrowPos = 1;
				MechSettings.secondaryColor = 1;
				break;
			case "sYellow":
				//sArrowPos = 2;
				MechSettings.secondaryColor = 2;
				break;
			case "sBlue":
				//sArrowPos = 3;
				MechSettings.secondaryColor = 3;
				break;
			case "start":
				Application.LoadLevel("MechLevel");
				break;
		}
	}
}

﻿using UnityEngine;
using System.Collections; 

public class MainMenu : MonoBehaviour {

	private int buttonNum = 3;
	private float sinNum = 0f;

	public Texture aTexture;

	void clickedYO(){
		Debug.Log("Hello World!");
	}

	void Start(){
		//Debug.Log("Hello World!");
	}

	void OnGUI () {
		// Make a background box
		//GUI.Box(new Rect(10,10,100,90), "Loader Menu");

		GUI.DrawTexture(new Rect((Mathf.Cos(sinNum)*50)+100,(Mathf.Sin(sinNum)*50)+100,80,20), aTexture, ScaleMode.ScaleToFit, true, 0);
		
		// Make the first button. If it is pressed, Application.Loadlevel (1) will be executed
		if(GUI.Button(new Rect((Mathf.Cos(sinNum)*50)+100,(Mathf.Sin(sinNum)*50)+100,80,20),"")){
			print ("You clicked me!");
			//Application.LoadLevel(2);

		}

		if(GUI.Button(new Rect(500,(Mathf.Sin(sinNum)*50)+100,80,20), "Lerp Button")){
			print ("You clicked me!");
			//Application.LoadLevel(2);
			
		}

		sinNum += 0.1f;
		if (sinNum > (2 * Mathf.PI)) {

			sinNum = 0;		
		}

		Debug.Log("TEST");
	}
	/*
	private void addAnotherButton(){
		if(GUI.Button(new Rect(20,(30*buttonNum)+10,80,20), "Level "+(buttonNum))) {
			Application.LoadLevel(buttonNum);
			buttonNum++;
		}
	}
	*/
}
﻿Shader "Custom/VertShader_Test2"
{
	Properties
	{
		_SpecularColor("Specular Color", Color) = (1.0,1.0,1.0,1.0)
		_Shininess("Shininess", Float) = 10
		_RimColor("Rimlight Color", Color) = (1.0,1.0,1.0,1.0)
		_RimPower("Rimlight Power", Range(.1,10.0)) = 3.0
	}

	SubShader
	{
		Pass
		{
			Tags { "LightMode" = "ForwardBase" }
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			
			uniform float4 _LightColor0;
			uniform float4 _SpecularColor;
			uniform float _Shininess;
			uniform float4 _RimColor;
			uniform float _RimPower;
			
			struct VertIn
			{
				float4 vertex : POSITION;
				fixed4 color : COLOR;
				float3 normal : NORMAL;
			};
			
			struct VertOut
			{
				float4 pos : SV_POSITION;
				float4 vert;
				float4 color : COLOR;
				float4 norm;
			};
			
			VertOut vert(VertIn v)
			{
				VertOut vOut;
				
				vOut.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				vOut.vert = vOut.pos;
				float3 normalDir = normalize(mul(float4(v.normal,0.0), _World2Object).xyz);
				vOut.norm = float4(normalDir,1.0);
				vOut.color = v.color;
				return vOut;
			}
			
			float4 frag(VertOut v) : COLOR0
			{
				float3 normalDir = v.norm.xyz;
				float3 lightDir = float3(0.0,0.0,0.0);
				float atten = 1.0;
				if(_WorldSpaceLightPos0.w == 0.0) //Direction light
				{
					lightDir = normalize(_WorldSpaceLightPos0.xyz);
				}
				if(_WorldSpaceLightPos0.w==1.0) //Point Light
				{
					float3 fragmentToLight = (_WorldSpaceLightPos0.xyz - v.vert.xyz);
					lightDir = normalize(fragmentToLight);
					atten = 1 / length(fragmentToLight);
				}
				float3 viewDir = normalize(_WorldSpaceCameraPos.xyz - v.vert.xyz);
				float3 diffuseRefl = atten * _LightColor0.xyz * max(0.0, dot(normalDir,lightDir));
				float3 specRefl = atten * 1.0 * _SpecularColor.xyz * max(0.0, dot(normalDir, lightDir)) * pow(max(0.0,dot(reflect(-lightDir, normalDir),viewDir)),_Shininess);
				float3 rim = 1 - saturate(dot(viewDir,normalDir));
				float3 rimColor = saturate(dot(viewDir,lightDir)) * pow(rim,_RimPower) * atten * _LightColor0.xyz * _RimColor.xyz;
				float3 totalColor = diffuseRefl+specRefl+UNITY_LIGHTMODEL_AMBIENT.xyz;
				return float4(totalColor * v.color.xyz,1.0);
			}
			
			ENDCG
		}
		
		Pass
		{
			Tags { "LightMode" = "ForwardAdd" }
			Blend ONE ONE
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			
			uniform float4 _LightColor0;
			uniform float4 _SpecularColor;
			uniform float _Shininess;
			uniform float4 _RimColor;
			uniform float _RimPower;
			
			struct VertIn
			{
				float4 vertex : POSITION;
				fixed4 color : COLOR;
				float3 normal : NORMAL;
			};
			
			struct VertOut
			{
				float4 pos : SV_POSITION;
				float4 vert;
				float4 color : COLOR;
				float4 norm;
			};
			
			VertOut vert(VertIn v)
			{
				VertOut vOut;
				
				vOut.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				vOut.vert = vOut.pos;
				float3 normalDir = normalize(mul(float4(v.normal,0.0), _World2Object).xyz);
				vOut.norm = float4(normalDir,1.0);
				vOut.color = v.color;
				return vOut;
			}
			
			float4 frag(VertOut v) : COLOR0
			{
				float3 normalDir = v.norm.xyz;
				float3 lightDir = float3(0.0,0.0,0.0);
				float atten = 1.0;
				if(_WorldSpaceLightPos0.w == 0.0) //Direction light
				{
					lightDir = normalize(_WorldSpaceLightPos0.xyz);
				}
				if(_WorldSpaceLightPos0.w==1.0) //Point Light
				{
					float3 fragmentToLight = (_WorldSpaceLightPos0.xyz - v.vert.xyz);
					lightDir = normalize(fragmentToLight);
					atten = 1 / length(fragmentToLight);
				}
				float3 viewDir = normalize(_WorldSpaceCameraPos.xyz - v.vert.xyz);
				float3 diffuseRefl = atten * _LightColor0.xyz * max(0.0, dot(normalDir,lightDir));
				float3 specRefl = atten * 1.0 * _SpecularColor.xyz * max(0.0, dot(normalDir, lightDir)) * pow(max(0.0,dot(reflect(-lightDir, normalDir),viewDir)),_Shininess);
				float3 rim = 1 - saturate(dot(viewDir,normalDir));
				float3 rimColor = saturate(dot(viewDir,lightDir)) * pow(rim,_RimPower) * atten * _LightColor0.xyz * _RimColor.xyz;
				float3 totalColor = diffuseRefl+specRefl;
				return float4(totalColor * v.color.xyz,1.0);
			}
			
			ENDCG
		}
	}
}
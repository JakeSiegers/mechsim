﻿Shader "Custom/VertShader_Test"
{
	Properties
	{
		_SpecularColor("Specular Color", Color) = (1.0,1.0,1.0,1.0)
		_Shininess("Shininess", Float) = 10
	}

	SubShader
	{
		Pass
		{
			Tags { "LightMode" = "ForwardBase" }
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			
			uniform float4 _LightColor0;
			uniform float4 _SpecularColor;
			uniform float _Shininess;
			
			struct VertIn
			{
				float4 vertex : POSITION;
				fixed4 color : COLOR;
				float3 normal : NORMAL;
			};
			
			struct VertOut
			{
				float4 pos : SV_POSITION;
				fixed4 color : COLOR;
			};
			
			VertOut vert(VertIn v)
			{
				VertOut vOut;
				float3 normalDir = normalize(mul(float4(v.normal,0.0), _World2Object).xyz);
				float3 lightDir = normalize(_WorldSpaceLightPos0.xyz);
				float3 viewDir = normalize(_WorldSpaceCameraPos.xyz - mul(_Object2World, v.vertex));
				float atten = 1.0;
				float3 diffuseRefl = atten * _LightColor0.xyz * v.color * max(0.0, dot(normalDir,lightDir));
				float3 specRefl = atten * 1.0 * _SpecularColor * max(0.0, dot(normalDir, lightDir)) * pow(max(0.0,dot(reflect(-lightDir, normalDir),viewDir)),_Shininess);
				vOut.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				float3 withAmbient = diffuseRefl+specRefl+UNITY_LIGHTMODEL_AMBIENT;
				vOut.color = float4(withAmbient * v.color,0.0);
				return vOut;
			}
			
			float4 frag(VertOut v) : COLOR0
			{
				return v.color;
			}
			
			ENDCG
		}
		
		Pass
		{
			Tags { "LightMode" = "ForwardAdd" }
			Blend One One
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			
			uniform float4 _LightColor0;
			
			struct VertIn
			{
				float4 vertex : POSITION;
				fixed4 color : COLOR;
				float3 normal : NORMAL;
			};
			
			struct VertOut
			{
				float4 pos : SV_POSITION;
				fixed4 color : COLOR;
			};
			
			VertOut vert(VertIn v)
			{
				VertOut vOut;
				float3 normalDir = normalize(mul(float4(v.normal,0.0), _World2Object).xyz);
				float3 lightDir = normalize(_WorldSpaceLightPos0.xyz);
				float3 diffuseRefl = max(0.0, dot(normalDir,lightDir));
				vOut.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				float3 lightedColor  = _LightColor0.xyz * diffuseRefl;
				vOut.color = float4(lightedColor * v.color,0.0);
				return vOut;
			}
			
			float4 frag(VertOut v) : COLOR0
			{
				return v.color;
			}
			
			ENDCG
		}
	}
}
﻿Shader "Custom/VertShader"
{
	SubShader
	{
		Pass
		{
			Tags { "LightMode" = "ForwardBase" }
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			
			uniform float4 _LightColor0;
			
			struct VertIn
			{
				float4 vertex : POSITION;
				fixed4 color : COLOR;
				float3 normal : NORMAL;
			};
			
			struct VertOut
			{
				float4 pos : SV_POSITION;
				fixed4 color : COLOR;
			};
			
			VertOut vert(VertIn v)
			{
				VertOut vOut;
				float3 normalDir = normalize(mul(float4(v.normal,0.0), _World2Object).xyz);
				float3 lightDir = normalize(_WorldSpaceLightPos0.xyz);
				float3 diffuseRefl = max(0.0, dot(normalDir,lightDir));
				vOut.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				float3 lightedColor  = _LightColor0.xyz * diffuseRefl;
				float3 withAmbient = lightedColor+UNITY_LIGHTMODEL_AMBIENT;
				vOut.color = float4(withAmbient * v.color,0.0);
				return vOut;
			}
			
			float4 frag(VertOut v) : COLOR0
			{
				return v.color;
			}
			
			ENDCG
		}
		
		Pass
		{
			Tags { "LightMode" = "ForwardAdd" }
			Blend One One
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			
			uniform float4 _LightColor0;
			
			struct VertIn
			{
				float4 vertex : POSITION;
				fixed4 color : COLOR;
				float3 normal : NORMAL;
			};
			
			struct VertOut
			{
				float4 pos : SV_POSITION;
				fixed4 color : COLOR;
			};
			
			VertOut vert(VertIn v)
			{
				VertOut vOut;
				float3 normalDir = normalize(mul(float4(v.normal,0.0), _World2Object).xyz);
				float3 lightDir = normalize(_WorldSpaceLightPos0.xyz);
				float3 diffuseRefl = max(0.0, dot(normalDir,lightDir));
				vOut.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				float3 lightedColor  = _LightColor0.xyz * diffuseRefl;
				vOut.color = float4(lightedColor * v.color,0.0);
				return vOut;
			}
			
			float4 frag(VertOut v) : COLOR0
			{
				return v.color;
			}
			
			ENDCG
		}
	}
}
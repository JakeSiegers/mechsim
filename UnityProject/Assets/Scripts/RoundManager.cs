﻿using UnityEngine;
using UnityEngine.UI; 
using System.Collections;
using System.Collections.Generic;

public class RoundManager : MonoBehaviour {
	private GameObject player;
	public GameObject deadzone;
	public GameObject mechPrefab;
	public GameObject enemyPrefab;
	public GameObject healthPrefab;
	public GameObject upgradePrefab;
	public GameObject explosion;
	private MechMove move;
	private MechHealth health; 
	private MechShooting1[] shootingScripts;
	private List<GameObject> playerSpawns= new List<GameObject>();
	private List<GameObject> enemySpawns= new List<GameObject>();
	private List<GameObject> objSpawns= new List<GameObject>();
	
	private bool roundStart = false;
	public float startCountFrom = 15f;
	private float startCountLeft = 0f;
	public Text starting;
	public Canvas startingCanvas;
	public Canvas hud;
	private float timer = 3f;
	public float objTimer = 60f;
	
	public bool isDead = false;
	public bool canRespawn = true;
	private float roundDuration = 0f;
	private float respawnWait = 5f;
	public int respawnCount = 3;
	public int enemyCount;
	private int enemyKillCount;
	
	// Use this for initialization
	void Start () {
		hud.enabled = false;
		startCountLeft = startCountFrom;
		foreach (GameObject spawn in GameObject.FindGameObjectsWithTag("playerSpawn"))
		{
			playerSpawns.Add (spawn);
		}
		
		foreach (GameObject spawn in GameObject.FindGameObjectsWithTag("enemySpawn"))
		{
			enemySpawns.Add (spawn);
		}
	
		foreach (GameObject spawn in GameObject.FindGameObjectsWithTag("objSpawn"))
		{
			objSpawns.Add (spawn);
		}
		
		spawnPlayer ();
	}
	
	
	void roundStarting()
	{
		
		startCountLeft -= Time.deltaTime;
		starting.text = "Starting In\n" + startCountLeft.ToString("F1");
		if (startCountLeft <= 0) 
		{
			roundStart = true;
			move.getControl();
			startingCanvas.enabled = false;
			hud.enabled = true;
		}
	}
	
	void playerRespawnWait()
	{
		
		respawnWait -= Time.deltaTime;
		starting.text = "Respawn In\n" + respawnWait.ToString("F1");
		if (respawnWait <= 0) 
		{
			respawnWait = 5f;
			respawn();
		}
	}
	// Update is called once per frame
	void Update () 
	{
		
		if (!roundStart) 
		{
			roundStarting ();
		}
		else if(isDead)
		{
			if(canRespawn && respawnCount>0)
				playerRespawnWait();
			else 
			{
				gameOver();
			}
		}
		timer -= Time.deltaTime;
		objTimer -= Time.deltaTime;
		if(enemyCount<20 && timer<0)
		{
			spawnEnemy();
		}
		if(objTimer<0)
		{
			spawnObj();
		}
		
	}
	
	void gameOver()
	{
		respawnWait -= Time.deltaTime;
		starting.text = "Game Over\n" + "Score: " + enemyKillCount;
		if (respawnWait <= 0) 
		{
			Screen.lockCursor = false;
			Application.LoadLevel("CharacterSelect");
		}
	}
	
	void spawnPlayer()
	{
		Transform point = playerSpawns [Random.Range (0, playerSpawns.Count-1)].transform;
		player = (GameObject)Instantiate (mechPrefab, point.position , Quaternion.identity);
		
		health = player.GetComponent<MechHealth>();
		move = player.GetComponent<MechMove> ();
		shootingScripts = player.GetComponentsInChildren<MechShooting1>();
		move.torso = "Torso_Heavy";
		move.primary = Color.red;
		switch(MechSettings.mechBase)
		{
			case 0:
				move.legs = "Legs_Jump";
				break;
			case 1:
				move.legs = "Legs_Wheel";
				break;
			default:
				move.legs="Legs_Jump";
				break;
		}

		switch(MechSettings.mechBody)
		{
		case 0:
			move.torso = "Torso_Lite";
			move.speedMod=1.25f;
			break;
		case 1:
			move.torso = "Torso_Med";
			break;
		case 3:
			move.torso="Torso_Heavy";
			move.speedMod=.75f;
			break;
		default:
			move.torso = "Torso_Med";
			break;
		}

		switch(MechSettings.primaryColor)
		{
			case 0:
				move.primary = Color.red;
				break;
			case 1:
				move.primary = Color.green;
				break;
			case 2:
				move.primary=Color.yellow;
				break;
			case 3:
				move.primary = Color.blue;
				break;
			default:
			move.primary = Color.red;
			break;
		}

		switch(MechSettings.secondaryColor)
		{
		case 0:
			move.secondary = Color.red;
			break;
		case 1:
			move.secondary = Color.green;
			break;
		case 2:
			move.secondary=Color.yellow;
			break;
		case 3:
			move.secondary = Color.blue;
			break;
		default:
			move.secondary = Color.red;
			break;
		}
		
	}
	
	void spawnEnemy()
	{
		Transform point = enemySpawns [Random.Range (0, enemySpawns.Count-1)].transform;
		Instantiate (enemyPrefab, point.position , Quaternion.identity);
		timer = 3f;
		enemyCount++;
		
		
	}

	void spawnObj()
	{
		foreach(GameObject objSpawn in objSpawns)
		{
//			int newInt = Random.Range (1, 10);
//			if(newInt%5 == 0)
//				Instantiate (upgradePrefab, objSpawn.transform.position , Quaternion.identity);
//			else
				Instantiate (healthPrefab, objSpawn.transform.position , Quaternion.identity);
		}
		
		objTimer = 60f;
		
		
	}
	
	public void die(GameObject dead)
	{
		if(player==dead)
		{
			move.loseControl();
			foreach(MechShooting1 mech in shootingScripts)
			{
				mech.enabled = false;
			}
			hud.enabled = false;
			isDead = true;
			startingCanvas.enabled = true;
			player.transform.position = deadzone.transform.position;
			health.enabled = false;
			player.rigidbody.velocity = Vector3.zero;
			respawnCount--;
		}
		else
		{
			Destroy (dead);
			enemyCount--;
			enemyKillCount++;
		}
		Instantiate(explosion, dead.transform.position, Quaternion.identity);
		Debug.Log("Enemies: " + enemyCount);
	}
	
	void respawn()
	{
		move.getControl();
		foreach(MechShooting1 mech in shootingScripts)
		{
			mech.enabled = true;
		}
		hud.enabled = true;
		startingCanvas.enabled = false;
		isDead = false;
		health.enabled = true;
		health.heal(1000f);
		Transform point = playerSpawns [Random.Range (0, playerSpawns.Count-1)].transform;
		player.transform.position = point.position;
		player.transform.rotation = Quaternion.identity;
		player.rigidbody.velocity = Vector3.zero;
	}
}

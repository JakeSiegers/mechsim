﻿using UnityEngine;
using System.Collections;

public class UpgradeStats : MonoBehaviour {
	private float timer = 0;
	public float deathSec = 59;	
	
	
	void OnCollisionEnter(Collision col)
	{
		if (col.gameObject.tag == "Player" ) 
		{
			col.gameObject.GetComponent<characterStatTester>().pickedUpUpgrade();
			Destroy (this.gameObject);
		}
	}
	
	void Update()
	{
		timer += Time.deltaTime;
		if(timer>=deathSec)
			Destroy (this.gameObject);
	}
}

﻿using UnityEngine;
using System.Collections;

public class Healer : MonoBehaviour 
{
	
	private float timer = 0;
	public float deathSec = 59;	

	public float healAmount = 20f;

	void OnCollisionEnter(Collision col)
	{
		if (col.gameObject.tag == "Player" ) 
		{
			col.gameObject.GetComponent<MechHealth>().heal (healAmount);
			Destroy (this.gameObject);
		}
	}

	void Update()
	{
		timer += Time.deltaTime;
		if(timer>=deathSec)
			Destroy (this.gameObject);
	}
}

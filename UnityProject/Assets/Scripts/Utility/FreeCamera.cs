﻿using UnityEngine;
using System.Collections;

public class FreeCamera : MonoBehaviour {

	public float movementSpeed, rotationSpeed;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(Input.GetKey(KeyCode.W))
		{
			transform.Translate(Vector3.forward * movementSpeed * Time.deltaTime);
		}
		if(Input.GetKey(KeyCode.S))
		{
			transform.Translate(Vector3.forward * -movementSpeed * Time.deltaTime);
		}
		if(Input.GetKey(KeyCode.A))
		{
			transform.Translate(Vector3.right * -movementSpeed * Time.deltaTime);
		}
		if(Input.GetKey(KeyCode.D))
		{
			transform.Translate(Vector3.right * movementSpeed * Time.deltaTime);
		}
		if(Input.GetKey(KeyCode.Space))
		{
			transform.Translate(Vector3.up * movementSpeed * Time.deltaTime);
		}
		if(Input.GetKey(KeyCode.LeftShift))
		{
			transform.Translate(Vector3.up * -movementSpeed * Time.deltaTime);
		}
		transform.Rotate(Vector3.up * rotationSpeed * Time.deltaTime * Input.GetAxis("Mouse X"));
		//transform.rotation = Quaternion.Euler(new Vector3(transform.rotation.x, transform.rotation.y, transform.rotation.z) + (Vector3)(Vector3.right * -rotationSpeed * Time.deltaTime * Input.GetAxis("Mouse Y")));
	}
}
